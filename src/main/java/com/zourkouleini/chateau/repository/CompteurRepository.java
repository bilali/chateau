package com.zourkouleini.chateau.repository;

import com.zourkouleini.chateau.model.Compteur;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CompteurRepository extends CommonRepository<Compteur> {
    List<Compteur> findByGerantId(Integer id);
}
