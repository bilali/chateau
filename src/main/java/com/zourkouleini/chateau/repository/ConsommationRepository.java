package com.zourkouleini.chateau.repository;

import com.zourkouleini.chateau.model.Consommation;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsommationRepository extends CommonRepository<Consommation> {
    List<Consommation> findByCompteurId(Integer compteurId);
}
