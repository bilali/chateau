package com.zourkouleini.chateau.controller;

import com.zourkouleini.chateau.dto.NewCompteurDto;
import com.zourkouleini.chateau.model.Compteur;
import com.zourkouleini.chateau.service.CompteurService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/compteurs")
@RestController
@RequiredArgsConstructor
public class CompteurController {

    private final CompteurService compteurService;

    @GetMapping
    public List<Compteur> getAll() {
        return this.compteurService.getAll();
    }

    @GetMapping("by-gerant/{gerantId}")
    public List<Compteur> getAll(@PathVariable Integer gerantId) {
        return this.compteurService.getByGerant(gerantId);
    }

    @PostMapping()
    public Compteur createCompteur(@RequestBody NewCompteurDto compteurDto) {
        Compteur compteur = Compteur.builder()
                .nom(compteurDto.getNom())
                .reference(compteurDto.getReference())
                .telephone(compteurDto.getTelephone())
                .build();

        return this.compteurService.createCompteur(compteur, compteurDto.getGerantId(),compteurDto.getClientId());
    }

    @PutMapping("/{id}")
    public Compteur updateCompteur(@PathVariable Integer id, @RequestBody NewCompteurDto compteurDto) {
        Compteur compteur = Compteur.builder()
                .nom(compteurDto.getNom())
                .reference(compteurDto.getReference())
                .telephone(compteurDto.getTelephone())
                .build();

        return this.compteurService.updateCompteur(id, compteur,compteurDto.getGerantId(),compteurDto.getClientId());
    }


    @GetMapping("/{id}")
    public Compteur getOne(@PathVariable Integer id) {
        return this.compteurService.getById(id);
    }

    @DeleteMapping("{id}")
    public void deleteOne(@PathVariable Integer id) {
        this.compteurService.delete(id);
    }
}
