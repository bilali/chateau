package com.zourkouleini.chateau.controller;

import com.zourkouleini.chateau.security.model.User;
import com.zourkouleini.chateau.security.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/api/v1/utilisateurs")
@RestController
@RequiredArgsConstructor
public class UtilisateurController {
    private final UserService userService;

    @PostMapping
    public User create(@RequestBody User user) {
        return this.userService.createAccount(user);
    }

    @PutMapping("{id}")
    public User update(@RequestBody User user, @PathVariable Integer id) {
        return this.userService.update(id, user);
    }

    @GetMapping("{id}")
    public User get( @PathVariable Integer id) {
        return this.userService.getById(id);
    }

    @DeleteMapping("{id}")
    public void delete(@PathVariable Integer id) {
        this.userService.delete(id);
    }
}
