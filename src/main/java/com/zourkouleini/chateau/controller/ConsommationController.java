package com.zourkouleini.chateau.controller;

import com.zourkouleini.chateau.dto.NewConsommationDto;
import com.zourkouleini.chateau.model.Consommation;
import com.zourkouleini.chateau.service.ConsommationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/v1/consommations")
@RestController
@RequiredArgsConstructor
public class ConsommationController {
    private final ConsommationService consommationService;

    @GetMapping
    public List<Consommation> getAll() {
        return this.consommationService.getAll();
    }

    @GetMapping("by-compteur/{compteurId}")
    public List<Consommation> getAll(@PathVariable Integer compteurId) {
        return this.consommationService.getAll(compteurId);
    }

    @PostMapping("/{compteurId}")
    public Consommation createConsommation(@PathVariable Integer compteurId, @RequestBody NewConsommationDto consommationDto) {
        Consommation consommation = Consommation.builder()
                .dateConsommation(consommationDto.getDateConsommation())
                .prix(consommationDto.getPrix())
                .quantite(consommationDto.getQuantite())
                .build();

        return this.consommationService.createConsommation(compteurId, consommation);
    }

    @PutMapping("/{id}")
    public Consommation updateConsommation(@PathVariable Integer id, @RequestBody NewConsommationDto consommationDto) {
        Consommation consommation = Consommation.builder()
                .dateConsommation(consommationDto.getDateConsommation())
                .prix(consommationDto.getPrix())
                .quantite(consommationDto.getQuantite())
                .build();

        return this.consommationService.updateConsommation(id, consommation);
    }


    @GetMapping("/{id}")
    public Consommation getOne(@PathVariable Integer id) {
        return this.consommationService.getById(id);
    }

    @DeleteMapping("{id}")
    public void deleteOne(@PathVariable Integer id) {
        this.consommationService.delete(id);
    }
}
