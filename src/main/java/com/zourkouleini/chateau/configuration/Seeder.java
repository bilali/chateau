package com.zourkouleini.chateau.configuration;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import com.zourkouleini.chateau.security.controller.Role;

import com.zourkouleini.chateau.security.model.User;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import  com.zourkouleini.chateau.security.repository.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;

@Component
@RequiredArgsConstructor
public class Seeder {

	private final PasswordEncoder passwordEncoder;
	private final UserRepository userRepository;


	@EventListener
	@Transactional
	public void seed(ApplicationReadyEvent event) {

		this.seedUsers();
	}

	@Transactional
	public void seedUsers() {


		boolean existsByUsername = this.userRepository
				.existsByEmail("admin@chateau.com");

		if (!existsByUsername) {

			User user = User.builder()
					.password(this.passwordEncoder.encode("password"))
					.nom("Admin").prenom("Admin")
					.email("admin@chateau.com")
					.role(Role.PROPRIETAIRE).build();
			this.userRepository.save(user);
		}

	}


}
