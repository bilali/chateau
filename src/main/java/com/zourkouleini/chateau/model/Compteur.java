package com.zourkouleini.chateau.model;

import com.zourkouleini.chateau.security.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@SuperBuilder
public class Compteur extends ModelSuperClass {
    private String nom;
    private String reference;
    private String telephone;

    @OneToMany(orphanRemoval = true, fetch = FetchType.EAGER)
    @Builder.Default
    private Set<Consommation> listConsommation = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "gerant_id")
    private User gerant;

    @ManyToOne
    @JoinColumn(name = "client_id")
    private User client;
}
