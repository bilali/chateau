package com.zourkouleini.chateau.service;

import com.zourkouleini.chateau.model.Compteur;
import com.zourkouleini.chateau.repository.CompteurRepository;
import com.zourkouleini.chateau.security.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import  com.zourkouleini.chateau.security.repository.UserRepository;
@Service
@RequiredArgsConstructor
public class CompteurService {
    private final CompteurRepository compteurRepository;
    private final  UserRepository userRepository;


    public List<Compteur> getAll() {
        return this.compteurRepository.findAll();
    }

    public Compteur createCompteur(Compteur compteur, Integer gerantId, Integer clientId) {
        Optional<User> gerant = userRepository.findById(gerantId);
        if(gerant.isPresent()) {
            compteur.setGerant(gerant.get());
        }

        Optional<User> client = userRepository.findById(clientId);
        if(client.isPresent()) {
            compteur.setClient(client.get());
        }

        return this.compteurRepository.save(compteur);
    }

    public Compteur getById(Integer id) {
        return this.compteurRepository.findById(id).orElse(null);
    }

    public Compteur updateCompteur(Integer id, Compteur compteur,Integer gerantId, Integer clientId) {

        Compteur compteurExistante = this.getById(id);

        compteurExistante.setNom(compteur.getNom());
        compteurExistante.setReference(compteur.getReference());
        compteurExistante.setTelephone(compteur.getTelephone());

        Optional<User> gerant = userRepository.findById(gerantId);
        if(gerant.isPresent()) {
            compteur.setGerant(gerant.get());
        }

        Optional<User> client = userRepository.findById(clientId);
        if(client.isPresent()) {
            compteur.setClient(client.get());
        }

        return this.compteurRepository.save(compteurExistante);
    }

    public void delete(Integer id) {
        this.compteurRepository.deleteById(id);
    }

    public List<Compteur> getByGerant(Integer gerantId) {
        return this.compteurRepository.findByGerantId(gerantId);
    }
}
