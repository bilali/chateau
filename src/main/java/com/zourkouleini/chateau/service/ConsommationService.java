package com.zourkouleini.chateau.service;

import com.zourkouleini.chateau.model.Compteur;
import com.zourkouleini.chateau.model.Consommation;
import com.zourkouleini.chateau.repository.CompteurRepository;
import com.zourkouleini.chateau.repository.ConsommationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ConsommationService {
    private final ConsommationRepository consommationRepository;
    private final CompteurRepository compteurRepository;

    public List<Consommation> getAll() {
        return this.consommationRepository.findAll();
    }

    public List<Consommation> getAll(Integer compteurId) {
        return this.consommationRepository.findByCompteurId(compteurId);
    }

    public Consommation createConsommation(Integer compteurId, Consommation consommation) {
        Compteur compteur = this.compteurRepository.findById(compteurId).orElse(null);
        consommation.setCompteur(compteur);
        return this.consommationRepository.save(consommation);
    }

    public Consommation getById(Integer id) {
        return this.consommationRepository.findById(id).orElse(null);
    }

    public Consommation updateConsommation(Integer id, Consommation consommation) {

        Consommation consommationExistante = this.getById(id);

        consommationExistante.setDateConsommation(consommation.getDateConsommation());
        consommationExistante.setPrix(consommation.getPrix());
        consommationExistante.setQuantite(consommation.getQuantite());

        return this.consommationRepository.save(consommationExistante);
    }

    public void delete(Integer id) {
        this.consommationRepository.deleteById(id);
    }
}
