package com.zourkouleini.chateau.dto;

import lombok.Data;

@Data
public class NewCompteurDto {
    String nom;
    String reference;
    String telephone;
    Integer gerantId;
    Integer clientId;
}
