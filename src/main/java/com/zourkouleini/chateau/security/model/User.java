package com.zourkouleini.chateau.security.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonProperty.Access;
import com.zourkouleini.chateau.security.controller.Role;
import com.zourkouleini.chateau.model.ModelSuperClass;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@SuperBuilder
@Entity
@Table(name = "account")
public class User extends ModelSuperClass {

    private String nom;
    private String prenom;
    private String email;
    private String telephone;

    @JsonProperty(access = Access.WRITE_ONLY)
    String password;
    @Builder.Default
    boolean isEnabled = true;

    Role role;

}
