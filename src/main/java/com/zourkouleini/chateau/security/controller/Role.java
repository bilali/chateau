package com.zourkouleini.chateau.security.controller;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.AllArgsConstructor;
import lombok.Getter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Getter
@AllArgsConstructor
public enum Role {

    @JsonProperty("0")
     PROPRIETAIRE(0,"PROPRIETAIRE"),
    @JsonProperty("1")
    GERANT(1, "GERANT"),
    @JsonProperty("2")
    CLIENT(2, "CLIENT");

    @JsonValue
    Integer code;

    String label;



    @Converter(autoApply = true)
    public static class RoleConverter implements AttributeConverter<Role, Integer> {

        @Override
        public Integer convertToDatabaseColumn(Role attribute) {
            return (attribute != null) ? attribute.getCode() : null;
        }

        @Override
        public Role convertToEntityAttribute(Integer dbData) {

            for (Role e : Role.values()) {
                if (e.getCode().equals(dbData)) {
                    return e;
                }
            }
            return null;
        }
    }
}
