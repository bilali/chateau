package com.zourkouleini.chateau.security.controller;

import com.zourkouleini.chateau.security.config.JwtTokenUtil;
import com.zourkouleini.chateau.security.model.JwtRequest;
import com.zourkouleini.chateau.security.model.User;
import com.zourkouleini.chateau.security.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "auth")
@RequiredArgsConstructor
@Slf4j
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;

    private final JwtTokenUtil jwtTokenUtil;

    private final UserService userDetailsService;

    @PostMapping(value = "login")
    public ResponseEntity<String> createAuthenticationToken(
            @RequestBody JwtRequest authenticationRequest) {
        try {

            authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(
                            authenticationRequest.getEmail(),
                            authenticationRequest.getPassword()));
            final UserDetails userDetails = userDetailsService
                    .loadUserByUsername(authenticationRequest.getEmail());
            final String token = jwtTokenUtil.generateToken(userDetails);

            log.info("{} has been logged in",
                    authenticationRequest.getEmail());
            return ResponseEntity.ok(token);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.debug(ExceptionUtils.getStackTrace(e));
            log.info("{} tried to log in", authenticationRequest.getEmail());
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).build();
        }

    }

    @PostMapping(value = "signup")
    public ResponseEntity<User> signup(@RequestBody User user) {
        try {

            user = this.userDetailsService.createAccount(user);
            log.info("User created for {} ", user.getEmail());
            return ResponseEntity.ok(user);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }

    }

    @GetMapping(value = "checkusername/{username}")
    public ResponseEntity<Boolean> checkUsername(
            @PathVariable String username) {
        try {
            boolean result = this.userDetailsService
                    .checkUsernameExistance(username);
            log.info("{} n'existe pas ", username);
            return ResponseEntity.ok(result);
        } catch (Exception e) {
            log.error(e.getMessage());
            log.error(ExceptionUtils.getStackTrace(e));
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
                    .build();
        }

    }
}
