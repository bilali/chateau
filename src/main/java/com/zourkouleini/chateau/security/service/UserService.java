package com.zourkouleini.chateau.security.service;

import com.zourkouleini.chateau.security.controller.Role;
import com.zourkouleini.chateau.security.model.User;
import com.zourkouleini.chateau.security.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username)
            throws UsernameNotFoundException {

        User user = this.userRepository.findByEmail(username);
        if (user == null) {
            throw new UsernameNotFoundException(
                    "No user with username : " + username + " exists!");
        }

        return new org.springframework.security.core.userdetails.User(
                user.getEmail(), user.getPassword(), user.isEnabled(), true,
                true, true, getAuthorities(user.getRole()));

    }

    private Collection<? extends GrantedAuthority> getAuthorities(Role role) {

        return getGrantedAuthorities(role.getLabel());
    }

    private List<GrantedAuthority> getGrantedAuthorities(String role) {
        List<GrantedAuthority> authorities = new ArrayList<>();

        authorities.add(new SimpleGrantedAuthority(role));

        return authorities;
    }

    public User createAccount(User user) {
        user.setPassword(this.passwordEncoder.encode(user.getPassword()));
        return this.userRepository.save(user);
    }

    public boolean checkUsernameExistance(String username) {
        return this.userRepository.existsByEmail(username);
    }

    public Page<User> getAll(Pageable pageable) {
        return this.userRepository.findAll(pageable);
    }

    public User update(Integer id, User user) {
        user.setId(id);
        return this.userRepository.save(user);
    }
    public User getById(Integer id) {

        return this.userRepository.findById(id).orElse(null);
    }

    public void delete(Integer id) {
        this.userRepository.deleteById(id);
    }
}
