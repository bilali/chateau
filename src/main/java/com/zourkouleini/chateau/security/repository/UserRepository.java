package com.zourkouleini.chateau.security.repository;

import com.zourkouleini.chateau.repository.CommonRepository;
import com.zourkouleini.chateau.security.model.User;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends CommonRepository<User> {

    User findByEmail(String username);

    boolean existsByEmail(String username);
}
